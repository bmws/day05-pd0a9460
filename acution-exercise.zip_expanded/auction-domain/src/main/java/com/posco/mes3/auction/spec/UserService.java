package com.posco.mes3.auction.spec;

import com.posco.mes3.auction.entity.User;

import java.util.List;

public interface UserService {
    //
    public abstract String registerUser(User user);
    public abstract List<User> findAllUsers();
    public abstract User findUser(String id);
}
