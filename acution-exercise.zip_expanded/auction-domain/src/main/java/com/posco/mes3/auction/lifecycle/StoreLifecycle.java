package com.posco.mes3.auction.lifecycle;

import com.posco.mes3.auction.store.BidStore;
import com.posco.mes3.auction.store.ItemStore;
import com.posco.mes3.auction.store.UserStore;

public interface StoreLifecycle {
    //
    public abstract UserStore requestUserStore();
    public abstract ItemStore requestItemStore();
    public abstract BidStore requestBidStore();
}
