package com.posco.mes3.auction.store;

import com.posco.mes3.auction.entity.Bid;

import java.util.List;

public interface BidStore {
    //
    public abstract void create(Bid bid);
    public abstract Bid retrieveById(String id);
    public abstract List<Bid> retrieveAllByItem(String itemId);
    public abstract List<Bid> retrieveAllByBidder(String bidderId);
}
