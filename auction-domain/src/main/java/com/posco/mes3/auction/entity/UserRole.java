package com.posco.mes3.auction.entity;

public enum UserRole {
    //
    Seller,
    Bidder;
}
