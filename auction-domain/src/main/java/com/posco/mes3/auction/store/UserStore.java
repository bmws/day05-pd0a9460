package com.posco.mes3.auction.store;

import com.posco.mes3.auction.entity.User;

import java.util.List;

public interface UserStore {
    //
    public abstract void createUser(User user);
    public abstract List<User> retrieveAllUsers();
    public abstract User retrieveUserById(String id);
    public abstract boolean existsUserByName(String name);
}
