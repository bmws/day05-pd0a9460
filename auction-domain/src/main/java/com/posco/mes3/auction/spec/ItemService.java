package com.posco.mes3.auction.spec;

import com.posco.mes3.auction.entity.Bid;
import com.posco.mes3.auction.entity.Item;

import java.util.List;

public interface ItemService {
    //
    public abstract String registerItem(Item item);
    public abstract Item findItem(String id);
    public abstract List<Item> findItemsBySeller(String sellerId);
}
