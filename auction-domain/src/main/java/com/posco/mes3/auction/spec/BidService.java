package com.posco.mes3.auction.spec;

import com.posco.mes3.auction.entity.Bid;
import com.posco.mes3.auction.entity.Item;

import java.util.List;

public interface BidService {
    //
    public abstract void bid(Bid bid);
    public abstract Bid findBidById(String id);
    public abstract List<Bid> findBidsByItem(String itemId);
    public abstract List<Bid> findBidsByBidder(String bidderId);
}
