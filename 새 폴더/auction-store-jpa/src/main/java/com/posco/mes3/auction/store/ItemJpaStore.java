package com.posco.mes3.auction.store;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.posco.mes3.auction.entity.IdName;
import com.posco.mes3.auction.entity.Item;
import com.posco.mes3.auction.entity.User;
import com.posco.mes3.auction.store.jpo.ItemJpo;
import com.posco.mes3.auction.store.jpo.UserJpo;
import com.posco.mes3.auction.store.repository.ItemRepository;
import com.posco.mes3.auction.store.repository.UserRepository;

@Repository
public class ItemJpaStore implements ItemStore{

	private ItemRepository itemRepository;

	
	
	
	public ItemJpaStore(ItemRepository itemRepository) {
		itemRepository = this.itemRepository;
	}

	@Override
	public void create(Item item) {
		// TODO Auto-generated method stub
		itemRepository.save(new ItemJpo( item));
	}

	@Override
	public Item retrieve(String id) {
		Optional<ItemJpo> find = itemRepository.findById(id);
		if(!find.isPresent()) {
			System.out.println("");
		}
		return find.get().toDomain();
	}

	@Override
	public List<Item> retrieveAllBySeller(String sellerId) {
		// TODO Auto-generated method stub
		//IdName id = new IdName();
		//id.setId(sellerId);
		List<ItemJpo> finds = itemRepository.findALLBySellerId(sellerId);


		/*List<Item> newitemJpos = new ArrayList<Item>();
		for (ItemJpo ItemJpo : finds) {
			newitemJpos.add(ItemJpo.toDomain());
		}
		return newitemJpos;
		*/
		
		return finds.stream()
				.map(ItemJpo::toDomain)
				.collect(Collectors.toList());
		
	}

	@Override
	public void update(Item item) {
		// TODO Auto-generated method stub
		itemRepository.save(new ItemJpo( item));
	}

}
