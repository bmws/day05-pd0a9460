package com.posco.mes3.auction.store.jpo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.posco.mes3.auction.entity.User;
import com.posco.mes3.auction.entity.UserRole;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "TB_USER")
@NoArgsConstructor
public class UserJpo {


	@Id
	private String id;
	private String name;

    //@Column(name = "USER_ROLE")
	@Transient
    private List<UserRole> roles;
    
	private String rolseJson;

	
	public UserJpo(User user) {
		//
		this.id = user.getId();
		this.name = user.getName();
		this.setRoles(user.getRoles());
	}
	
	

	public User toDomain() {
		//
		User user = new User(this.id);
		user.setName(this.name);
		user.setRoles(this.roles);
		//user.fromJson(user).toDomain();
		
		
		return user;
	}
}
