package com.posco.mes3.auction.store.jpo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.posco.mes3.auction.entity.Bid;
import com.posco.mes3.auction.entity.IdName;
import com.posco.mes3.auction.entity.Item;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "TB_BID")

public class BidJpo {

	@Id
    private String id;

	private String itemId;
    private String itemJson;
    private String bidderId;
    private String bidderJson;
    private int price;
    private long time;

    public BidJpo() {
    	
    }
    
	public BidJpo(Bid bid) {
		//
		this.id = bid.getId();
		this.itemId = bid.getItem().getId();
		this.itemJson = bid.getItem().toJson();
		this.bidderId = bid.getBidder().getId();
		this.bidderJson = bid.getBidder().toJson();
		this.price = bid.getPrice();
		this.time = bid.getTime();
		
		//this.roles = user.getRoles().toJson();
	}
	
	public Bid toDomain() {
		//
		Bid bid = new Bid(this.id);
		bid.setItem(IdName.fromJson(itemJson));
		bid.setBidder(IdName.fromJson(bidderJson));
		bid.setPrice(this.price);
		bid.setTime(this.time);
		
		return bid;
	}
	
	
}
