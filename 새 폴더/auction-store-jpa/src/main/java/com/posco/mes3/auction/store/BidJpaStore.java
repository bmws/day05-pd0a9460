package com.posco.mes3.auction.store;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.posco.mes3.auction.entity.Bid;
import com.posco.mes3.auction.entity.IdName;
import com.posco.mes3.auction.entity.Item;
import com.posco.mes3.auction.store.repository.ItemRepository;
import com.posco.mes3.auction.store.jpo.BidJpo;
import com.posco.mes3.auction.store.jpo.ItemJpo;
import com.posco.mes3.auction.store.repository.BidRepository;

@Repository
public class BidJpaStore implements BidStore {

	private BidRepository bidRepository;
	@Override
	public void create(Bid bid) {
		// TODO Auto-generated method stub

		bidRepository.save(new BidJpo( bid));
	}
	

	public BidJpaStore(BidRepository bidRepository) {
		bidRepository = this.bidRepository;
	}


	@Override
	public Bid retrieveById(String id) {
		// TODO Auto-generated method stub
		Optional<BidJpo> find = bidRepository.findById(id);
		
		return find.get().toDomain();
	}

	@Override
	public List<Bid> retrieveAllByItem(String itemId) {
		// TODO Auto-generated method stub
		IdName id = new IdName();
		id.setId(itemId);
		List<BidJpo> find = bidRepository.findALLByItemId(itemId);

		/*List<Bid> bids = new ArrayList<Bid>();
		for(BidJpo item : findSeller) {
			bids.add(item.toDomain());
		}
		
		return bids;*/
		return find.stream()
				.map(BidJpo::toDomain)
				.collect(Collectors.toList());
		
	}

	@Override
	public List<Bid> retrieveAllByBidder(String bidderId) {
		// TODO Auto-generated method stub
		List<BidJpo> find = bidRepository.findAllByBidderId(bidderId);

		return find.stream()
				.map(BidJpo::toDomain)
				.collect(Collectors.toList());
		
	}


	//private bidRe slabRepository;
}
