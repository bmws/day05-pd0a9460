package com.posco.mes3.auction.store;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.mockito.internal.matchers.Find;
import org.springframework.stereotype.Repository;

import com.posco.mes3.auction.entity.Bid;
import com.posco.mes3.auction.entity.User;
import com.posco.mes3.auction.store.jpo.BidJpo;
import com.posco.mes3.auction.store.jpo.UserJpo;
import com.posco.mes3.auction.store.repository.UserRepository;

@Repository
public class UserJpaStore implements UserStore{

	private UserRepository userRepository;

	
	public UserJpaStore(UserRepository userRepository) {
		userRepository = this.userRepository;
	}

	@Override
	public void createUser(User user) {
		// TODO Auto-generated method stub
		userRepository.save(new UserJpo(user));
	}

	@Override
	public List<User> retrieveAllUsers() {
		List<UserJpo> findAll = userRepository.findAll();
		List<User> users = new ArrayList<User>();
		for(UserJpo user : findAll) {
			users.add(user.toDomain());
		}
		
		
		
		
		return users;
	}

	@Override
	public User retrieveUserById(String id) {
		// TODO Auto-generated method stub
		Optional<UserJpo> find = userRepository.findById(id);
		if(!find.isPresent()) {
			System.out.println("");
		}
		return find.get().toDomain();
	}

	@Override
	public boolean existsUserByName(String name) {
		// TODO Auto-generated method stub

		boolean isExist = false;
		
		Optional<UserJpo> find = userRepository.findByName(name);
		if(find.isPresent()) {
			isExist = true;
		}
		return isExist;
	}

}
