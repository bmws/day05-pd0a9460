package com.posco.mes3.auction.rest;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.posco.mes3.auction.entity.Bid;
import com.posco.mes3.auction.entity.Item;
import com.posco.mes3.auction.entity.User;
import com.posco.mes3.auction.spec.BidService;
import com.posco.mes3.auction.spec.ItemService;
import com.posco.mes3.auction.spec.UserService;

@RestController
@RequestMapping(value = "/action")
public class actionResource implements UserService, BidService, ItemService{

	private final UserService userService;
	private final BidService bidService;
	private final ItemService itemService;

	public actionResource(UserService userService, BidService bidService, ItemService itemService) {
		this.userService = userService;
		this.bidService = bidService;
		this.itemService = itemService;
		
	}
	

	/*item*/
	@PostMapping(value = {"/item", "/item/"})
	public String registerItem(@RequestBody Item item) {
		// TODO Auto-generated method stub
		return itemService.registerItem(item);
	}

	@GetMapping(value = {"/item/{id}"})
	public Item findItem(@PathVariable(value = "id") String id) {
		// TODO Auto-generated method stub
		return itemService.findItem(id);
	}

	@GetMapping(value = {"/Seller/{id}"})
	public List<Item> findItemsBySeller(@PathVariable(value = "id") String sellerId) {
		// TODO Auto-generated method stub
		return itemService.findItemsBySeller(sellerId);
	}

	/*bid*/
	@GetMapping(value = {"/bid"})
	public void bid(@RequestBody Bid bid) {
		// TODO Auto-generated method stub
		bidService.bid(bid);
	}

	@GetMapping(value = {"/bid/{id}"})
	public Bid findBidById(@PathVariable(value = "id") String id) {
		// TODO Auto-generated method stub
		return bidService.findBidById(id);
	}

	@GetMapping(value = {"/bid/item/{id}"})
	public List<Bid> findBidsByItem(@PathVariable(value = "id") String itemId) {
		// TODO Auto-generated method stub
		return bidService.findBidsByItem(itemId);
	}

	@GetMapping(value = {"/bid/bidder/{id}"})
	public List<Bid> findBidsByBidder(@PathVariable(value = "id") String bidderId) {
		// TODO Auto-generated method stub
		return bidService.findBidsByBidder(bidderId);
	}

	/*user*/
	
	@PostMapping(value = {"/user", "/user/"})
	public String registerUser(@RequestBody User user) {
		// TODO Auto-generated method stub
		return userService.registerUser(user);
	}

	@GetMapping(value = {"/user"})
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return userService.findAllUsers();
	}

	@GetMapping(value = {"/user/{id}"})
	public User findUser(@PathVariable(value = "id") String id) {
		// TODO Auto-generated method stub
		return userService.findUser(id);
	}

}
