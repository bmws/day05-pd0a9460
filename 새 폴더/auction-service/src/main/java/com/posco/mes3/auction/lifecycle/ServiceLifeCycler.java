package com.posco.mes3.auction.lifecycle;

import org.springframework.stereotype.Component;

import com.posco.mes3.auction.logic.BidLogic;
import com.posco.mes3.auction.logic.ItemLogic;
import com.posco.mes3.auction.logic.UserLogic;
import com.posco.mes3.auction.spec.BidService;
import com.posco.mes3.auction.spec.ItemService;
import com.posco.mes3.auction.spec.UserService;

@Component
public class ServiceLifeCycler implements ServiceLifecycle{

	//private final ServiceLifecycle serviceLifecycle;
	
	private final StoreLifecycle storeLifecycle;
	
	public ServiceLifeCycler(StoreLifecycle storeLifecycle) {
		this.storeLifecycle = storeLifecycle;
	}

	public UserService requestUserService() {
		// TODO Auto-generated method stub
		return new UserLogic(storeLifecycle) ;
	}

	public ItemService requestItemService() {
		// TODO Auto-generated method stub
		return new ItemLogic(storeLifecycle);
	}

	public BidService requestBidService() {
		// TODO Auto-generated method stub
		return new BidLogic(storeLifecycle);
	}

	
}
