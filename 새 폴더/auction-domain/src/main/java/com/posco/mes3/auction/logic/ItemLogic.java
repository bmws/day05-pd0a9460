package com.posco.mes3.auction.logic;

import com.posco.mes3.auction.entity.Item;
import com.posco.mes3.auction.entity.User;
import com.posco.mes3.auction.entity.UserRole;
import com.posco.mes3.auction.exception.HasNotRoleException;
import com.posco.mes3.auction.lifecycle.StoreLifecycle;
import com.posco.mes3.auction.spec.ItemService;
import com.posco.mes3.auction.store.ItemStore;
import com.posco.mes3.auction.store.UserStore;

import java.util.List;

public class ItemLogic implements ItemService {
    //
    private final UserStore userStore;
    private final ItemStore itemStore;

    public ItemLogic(StoreLifecycle storeLifecycle) {
        //
        this.userStore = storeLifecycle.requestUserStore();
        this.itemStore = storeLifecycle.requestItemStore();
    }

    public String registerItem(Item item) {
        //
        User bidder = userStore.retrieveUserById(item.getSeller().getId());
        //if (!bidder.isSeller())
        if (!bidder.hasRole(UserRole.Seller)) {
            throw new HasNotRoleException(String.format("You are not seller."));
        }

        this.itemStore.create(item);
        return item.getId();
    }

    public Item findItem(String id) {
        //
        return this.itemStore.retrieve(id);
    }

    public List<Item> findItemsBySeller(String sellerId) {
        //
        return this.itemStore.retrieveAllBySeller(sellerId);
    }
}
