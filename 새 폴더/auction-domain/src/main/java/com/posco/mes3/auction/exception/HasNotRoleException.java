package com.posco.mes3.auction.exception;

public class HasNotRoleException extends RuntimeException {
    //
    public HasNotRoleException(String message) {
        //
        super(message);
    }
}
