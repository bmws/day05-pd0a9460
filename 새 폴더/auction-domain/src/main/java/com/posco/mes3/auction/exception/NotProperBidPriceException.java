package com.posco.mes3.auction.exception;

public class NotProperBidPriceException extends RuntimeException {
    //
    public NotProperBidPriceException(String message) {
        //
        super(message);
    }
}
