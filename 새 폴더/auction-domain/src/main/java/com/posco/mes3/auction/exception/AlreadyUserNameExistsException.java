package com.posco.mes3.auction.exception;

public class AlreadyUserNameExistsException extends RuntimeException {
    //
    public AlreadyUserNameExistsException(String message) {
        //
        super(message);
    }
}
