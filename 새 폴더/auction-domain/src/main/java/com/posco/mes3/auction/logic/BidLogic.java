package com.posco.mes3.auction.logic;

import com.posco.mes3.auction.entity.Bid;
import com.posco.mes3.auction.entity.Item;
import com.posco.mes3.auction.entity.User;
import com.posco.mes3.auction.entity.UserRole;
import com.posco.mes3.auction.exception.HasNotRoleException;
import com.posco.mes3.auction.exception.NotProperBidPriceException;
import com.posco.mes3.auction.lifecycle.StoreLifecycle;
import com.posco.mes3.auction.spec.BidService;
import com.posco.mes3.auction.store.BidStore;
import com.posco.mes3.auction.store.ItemStore;
import com.posco.mes3.auction.store.UserStore;

import java.util.List;

public class BidLogic implements BidService {
    //
    private final UserStore userStore;
    private final ItemStore itemStore;
    private final BidStore bidStore;

    public BidLogic(StoreLifecycle storeLifecycle) {
        //
        this.userStore = storeLifecycle.requestUserStore();
        this.itemStore = storeLifecycle.requestItemStore();
        this.bidStore = storeLifecycle.requestBidStore();
    }

    public void bid(Bid bid) {
        //
        User bidder = userStore.retrieveUserById(bid.getBidder().getId());
        //if (!bidder.isBidder())
        if (!bidder.hasRole(UserRole.Bidder)) {
            throw new HasNotRoleException(String.format("You are not bidder."));
        }

        Item item = itemStore.retrieve(bid.getItem().getId());
        if (!item.isBiddablePrice(bid.getPrice())) {
            throw new NotProperBidPriceException(String.format("The bid unit is +%d.", item.getPricePolicy().getPriceStep()));
        }

        item.setCurrentPrice(bid.getPrice());
        this.bidStore.create(bid);
        this.itemStore.update(item);
    }

    public Bid findBidById(String id) {
        //
        return this.bidStore.retrieveById(id);
    }

    public List<Bid> findBidsByItem(String itemId) {
        //
        return bidStore.retrieveAllByItem(itemId);
    }

    public List<Bid> findBidsByBidder(String bidderId) {
        //
        return bidStore.retrieveAllByBidder(bidderId);
    }

}
