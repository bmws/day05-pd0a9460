package com.posco.mes3.auction.lifecycle;

import com.posco.mes3.auction.spec.*;
import com.posco.mes3.auction.store.BidStore;
import com.posco.mes3.auction.store.ItemStore;
import com.posco.mes3.auction.store.UserStore;

public interface ServiceLifecycle {
    //
    public abstract UserService requestUserService();
    public abstract ItemService requestItemService();
    public abstract BidService requestBidService();

}
