package com.posco.mes3.auction.logic;

import com.posco.mes3.auction.entity.User;
import com.posco.mes3.auction.exception.AlreadyUserNameExistsException;
import com.posco.mes3.auction.lifecycle.StoreLifecycle;
import com.posco.mes3.auction.spec.UserService;
import com.posco.mes3.auction.store.UserStore;

import java.util.List;

public class UserLogic implements UserService {
    //
    private final UserStore userStore;

    public UserLogic(StoreLifecycle storeLifecycle) {
        //
        this.userStore = storeLifecycle.requestUserStore();
    }

    public String registerUser(User user) {
        //
        if (userStore.existsUserByName(user.getName())) {
            throw new AlreadyUserNameExistsException(String.format("User(%s) already exists", user.getName()));
        }

        this.userStore.createUser(user);
        return user.getId();
    }

    public List<User> findAllUsers() {
        //
        return this.userStore.retrieveAllUsers();
    }

    public User findUser(String id) {
        //
        return this.userStore.retrieveUserById(id);
    }
}
