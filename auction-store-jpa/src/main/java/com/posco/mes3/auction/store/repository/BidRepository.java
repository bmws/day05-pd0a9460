package com.posco.mes3.auction.store.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.posco.mes3.auction.store.jpo.BidJpo;
import com.posco.mes3.auction.store.jpo.UserJpo;
import com.posco.mes3.auction.entity.IdName;
import java.util.List;

public interface BidRepository extends JpaRepository<BidJpo, String> {
	
	List<BidJpo> findAllByBidderId(String bidderId);

	List<BidJpo> findALLByItemId(String itemId);
	

	
}
