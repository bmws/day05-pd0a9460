package com.posco.mes3.auction.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.posco.mes3.auction.entity.User;
import com.posco.mes3.auction.store.jpo.BidJpo;
import com.posco.mes3.auction.store.jpo.UserJpo;
import java.lang.String;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserJpo, String> {

	
	Optional<UserJpo> findByName(String name);
	
	List<UserJpo> findAll();
	
}
