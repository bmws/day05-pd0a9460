package com.posco.mes3.auction.store.jpo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.posco.mes3.auction.entity.Bid;
import com.posco.mes3.auction.entity.IdName;
import com.posco.mes3.auction.entity.Item;
import com.posco.mes3.auction.entity.PricePolicy;
import com.posco.mes3.auction.entity.User;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "TB_ITEM")
@NoArgsConstructor
public class ItemJpo {

	@Id
    private String id;
    private String name;
    private String description;
    
    @Column(name = "PRICE_STPE")
    private int priceStep;
    
    @Column(name = "START_PRICE")
    private int startPrice;
    
    @Transient
    private PricePolicy pricePolicy;
    
    private int currentPrice;

    private String sellerId;

    @Transient
    private IdName seller;
    

    //private transient List<Bid> bids;

    private String bidsJson;
    
    
    
	public ItemJpo(Item item) {
		//
		this.id = item.getId();
		//this.bids = item.getBids();
    	this.bidsJson = new Gson().toJson(item.getBids());
		this.currentPrice = item.getCurrentPrice();
		this.description = item.getDescription();
		this.name = item.getName();
		this.pricePolicy = item.getPricePolicy();
		this.seller = item.getSeller();
		this.sellerId = item.getSellerId();
	}
	
	public Item toDomain() {
		//
		Item item = new Item(this.id);
		item.setName(this.name);
		item.setDescription(this.description);
		item.setPricePolicy(this.pricePolicy);
		item.setCurrentPrice(this.currentPrice);
		item.setSeller(this.seller);
		
		item.setSellerId(this.sellerId);
		
    	item.setBids(new Gson().fromJson(this.getBidsJson(), new TypeToken<List<Bid>>(){}.getType()));
		
		return item;
	}
}
