package com.posco.mes3.auction.lifecycle;

import org.springframework.stereotype.Component;

import com.posco.mes3.auction.store.BidStore;
import com.posco.mes3.auction.store.ItemStore;
import com.posco.mes3.auction.store.UserStore;

@Component
public class StoreLifecycler implements StoreLifecycle{


	//private final StoreLifecycle storeLifecycle;
	
	
	private final BidStore bidStore;
	private final UserStore userStore;
	private final ItemStore itemStore;
	
	
	/*public StoreLifecycler(BidStore bidStore, UserStore userStore, ItemStore itemStore) {
		
		this.bidStore = bidStore;
		this.userStore = userStore;
		this.itemStore = itemStore;
	}
	*/
	

	public StoreLifecycler(UserStore userStore, ItemStore itemStore, BidStore bidStore) {

		this.bidStore = bidStore;
		this.userStore = userStore;
		this.itemStore = itemStore;
		// TODO Auto-generated constructor stub
	}

	@Override
	public UserStore requestUserStore() {
		// TODO Auto-generated method stub
		return userStore;
	}

	@Override
	public ItemStore requestItemStore() {
		// TODO Auto-generated method stub
		return itemStore;
	}

	@Override
	public BidStore requestBidStore() {
		// TODO Auto-generated method stub
		return bidStore;
	}

	
}
